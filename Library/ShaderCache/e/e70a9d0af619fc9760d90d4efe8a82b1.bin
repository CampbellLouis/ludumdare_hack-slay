<Q                         _ADDITIONAL_LIGHTS      4%  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct _LightBuffer_Type
{
    float4 _MainLightPosition;
    float4 _MainLightColor;
    float4 _AdditionalLightsCount;
    float4 _AdditionalLightsPosition[16];
    float4 _AdditionalLightsColor[16];
    float4 _AdditionalLightsAttenuation[16];
    float4 _AdditionalLightsSpotDir[16];
};

struct UnityPerCamera_Type
{
    float4 _Time;
    float4 _SinTime;
    float4 _CosTime;
    float4 unity_DeltaTime;
    float3 _WorldSpaceCameraPos;
    float4 _ProjectionParams;
    float4 _ScreenParams;
    float4 _ZBufferParams;
    float4 unity_OrthoParams;
};

struct UnityPerDraw_Type
{
    float4 hlslcc_mtx4x4unity_ObjectToWorld[4];
    float4 hlslcc_mtx4x4unity_WorldToObject[4];
    float4 unity_LODFade;
    float4 unity_WorldTransformParams;
    float4 unity_LightData;
    float4 unity_LightIndices[2];
    float4 unity_SpecCube0_HDR;
    float4 unity_LightmapST;
    float4 unity_DynamicLightmapST;
    float4 unity_SHAr;
    float4 unity_SHAg;
    float4 unity_SHAb;
    float4 unity_SHBr;
    float4 unity_SHBg;
    float4 unity_SHBb;
    float4 unity_SHC;
};

struct UnityPerMaterial_Type
{
    float4 Color_19C6C157;
    float2 Vector2_6925FF1E;
    float Vector1_6DC2C68C;
    float4 Color_E448CF9F;
};

struct Mtl_FragmentIn
{
    float3 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD7 [[ user(TEXCOORD7) ]] ;
    float4 TEXCOORD8 [[ user(TEXCOORD8) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant _LightBuffer_Type& _LightBuffer [[ buffer(0) ]],
    constant UnityPerCamera_Type& UnityPerCamera [[ buffer(1) ]],
    constant UnityPerDraw_Type& UnityPerDraw [[ buffer(2) ]],
    constant UnityPerMaterial_Type& UnityPerMaterial [[ buffer(3) ]],
    sampler samplerunity_SpecCube0 [[ sampler (0) ]],
    sampler samplerTexture2D_48882336 [[ sampler (1) ]],
    texturecube<float, access::sample > unity_SpecCube0 [[ texture(0) ]] ,
    texture2d<float, access::sample > Texture2D_48882336 [[ texture(1) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    float3 u_xlat1;
    float4 u_xlat2;
    float3 u_xlat3;
    float3 u_xlat4;
    float4 u_xlat5;
    float3 u_xlat6;
    float3 u_xlat7;
    float3 u_xlat8;
    float3 u_xlat11;
    float u_xlat20;
    float u_xlat24;
    float u_xlat27;
    int u_xlati27;
    float u_xlat28;
    bool u_xlatb28;
    float u_xlat29;
    float u_xlat30;
    int u_xlati30;
    float u_xlat31;
    int u_xlati31;
    bool u_xlatb31;
    float u_xlat32;
    bool u_xlatb32;
    float u_xlat33;
    float u_xlat34;
    u_xlat0.x = dot(input.TEXCOORD4.xyz, input.TEXCOORD4.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * input.TEXCOORD4.xyz;
    u_xlat27 = dot(input.TEXCOORD7.xyz, input.TEXCOORD7.xyz);
    u_xlat27 = rsqrt(u_xlat27);
    u_xlat1.xyz = float3(u_xlat27) * input.TEXCOORD7.xyz;
    u_xlat28 = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat28 = clamp(u_xlat28, 0.0f, 1.0f);
    u_xlat28 = (-u_xlat28) + 1.0;
    u_xlat2.x = u_xlat28 * u_xlat28;
    u_xlat28 = u_xlat28 * u_xlat2.x;
    u_xlat11.xy = input.TEXCOORD8.xy / input.TEXCOORD8.ww;
    u_xlat29 = UnityPerCamera._Time.y * UnityPerMaterial.Vector1_6DC2C68C;
    u_xlat11.xy = fma(u_xlat11.xy, UnityPerMaterial.Vector2_6925FF1E.xyxx.xy, float2(u_xlat29));
    u_xlat11.xyz = Texture2D_48882336.sample(samplerTexture2D_48882336, u_xlat11.xy).xyz;
    u_xlat3.xyz = (-u_xlat11.xyz) + float3(1.0, 1.0, 1.0);
    u_xlat3.xyz = u_xlat3.xyz * UnityPerMaterial.Color_19C6C157.xyz;
    u_xlat3.xyz = fma(UnityPerMaterial.Color_E448CF9F.xyz, float3(u_xlat28), u_xlat3.xyz);
    u_xlat28 = dot(UnityPerCamera._Time.yy, float2(12.9898005, 78.2330017));
    u_xlat28 = sin(u_xlat28);
    u_xlat28 = u_xlat28 * 43758.5469;
    u_xlat28 = fract(u_xlat28);
    u_xlatb28 = 0.899999976<u_xlat28;
    u_xlat28 = (u_xlatb28) ? 1.0 : 0.800000012;
    u_xlat4.xyz = UnityPerMaterial.Color_19C6C157.xyz * float3(0.959999979, 0.959999979, 0.959999979);
    u_xlat20 = dot((-u_xlat1.xyz), u_xlat0.xyz);
    u_xlat20 = u_xlat20 + u_xlat20;
    u_xlat5.xyz = fma(u_xlat0.xyz, (-float3(u_xlat20)), (-u_xlat1.xyz));
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat5 = unity_SpecCube0.sample(samplerunity_SpecCube0, u_xlat5.xyz, level(4.05000019));
    u_xlat20 = u_xlat5.w + -1.0;
    u_xlat20 = fma(UnityPerDraw.unity_SpecCube0_HDR.w, u_xlat20, 1.0);
    u_xlat20 = max(u_xlat20, 0.0);
    u_xlat20 = log2(u_xlat20);
    u_xlat20 = u_xlat20 * UnityPerDraw.unity_SpecCube0_HDR.y;
    u_xlat20 = exp2(u_xlat20);
    u_xlat20 = u_xlat20 * UnityPerDraw.unity_SpecCube0_HDR.x;
    u_xlat5.xyz = u_xlat5.xyz * float3(u_xlat20);
    u_xlat5.xyz = u_xlat5.xyz * float3(0.941176474, 0.941176474, 0.941176474);
    u_xlat2.x = fma(u_xlat2.x, 0.5, 0.0399999991);
    u_xlat2.xzw = u_xlat2.xxx * u_xlat5.xyz;
    u_xlat2.xzw = fma(input.TEXCOORD0.xyz, u_xlat4.xyz, u_xlat2.xzw);
    u_xlat30 = dot(u_xlat0.xyz, _LightBuffer._MainLightPosition.xyz);
    u_xlat30 = clamp(u_xlat30, 0.0f, 1.0f);
    u_xlat30 = u_xlat30 * UnityPerDraw.unity_LightData.z;
    u_xlat5.xyz = float3(u_xlat30) * _LightBuffer._MainLightColor.xyz;
    u_xlat6.xyz = fma(input.TEXCOORD7.xyz, float3(u_xlat27), _LightBuffer._MainLightPosition.xyz);
    u_xlat27 = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat27 = max(u_xlat27, 1.17549435e-38);
    u_xlat27 = rsqrt(u_xlat27);
    u_xlat6.xyz = float3(u_xlat27) * u_xlat6.xyz;
    u_xlat27 = dot(u_xlat0.xyz, u_xlat6.xyz);
    u_xlat27 = clamp(u_xlat27, 0.0f, 1.0f);
    u_xlat30 = dot(_LightBuffer._MainLightPosition.xyz, u_xlat6.xyz);
    u_xlat30 = clamp(u_xlat30, 0.0f, 1.0f);
    u_xlat27 = u_xlat27 * u_xlat27;
    u_xlat27 = fma(u_xlat27, -0.9375, 1.00001001);
    u_xlat30 = u_xlat30 * u_xlat30;
    u_xlat27 = u_xlat27 * u_xlat27;
    u_xlat30 = max(u_xlat30, 0.100000001);
    u_xlat27 = u_xlat27 * u_xlat30;
    u_xlat27 = u_xlat27 * 3.0;
    u_xlat27 = 0.0625 / u_xlat27;
    u_xlat6.xyz = fma(float3(u_xlat27), float3(0.0399999991, 0.0399999991, 0.0399999991), u_xlat4.xyz);
    u_xlat2.xzw = fma(u_xlat6.xyz, u_xlat5.xyz, u_xlat2.xzw);
    u_xlat27 = min(_LightBuffer._AdditionalLightsCount.x, UnityPerDraw.unity_LightData.y);
    u_xlati27 = int(u_xlat27);
    u_xlat5.xyz = u_xlat2.xzw;
    u_xlati30 = 0x0;
    while(true){
        u_xlatb31 = u_xlati30>=u_xlati27;
        if(u_xlatb31){break;}
        u_xlat31 = float(u_xlati30);
        u_xlatb32 = u_xlat31<2.0;
        u_xlat6.xy = (bool(u_xlatb32)) ? UnityPerDraw.unity_LightIndices[0].xy : UnityPerDraw.unity_LightIndices[0].zw;
        u_xlat24 = u_xlat31 + -2.0;
        u_xlat31 = (u_xlatb32) ? u_xlat31 : u_xlat24;
        u_xlatb31 = u_xlat31<1.0;
        u_xlat31 = (u_xlatb31) ? u_xlat6.x : u_xlat6.y;
        u_xlati31 = int(u_xlat31);
        u_xlat6.xyz = (-input.TEXCOORD3.xyz) + _LightBuffer._AdditionalLightsPosition[u_xlati31].xyz;
        u_xlat32 = dot(u_xlat6.xyz, u_xlat6.xyz);
        u_xlat32 = max(u_xlat32, 6.10351562e-05);
        u_xlat33 = rsqrt(u_xlat32);
        u_xlat7.xyz = float3(u_xlat33) * u_xlat6.xyz;
        u_xlat34 = float(1.0) / float(u_xlat32);
        u_xlat32 = u_xlat32 * _LightBuffer._AdditionalLightsAttenuation[u_xlati31].x;
        u_xlat32 = fma((-u_xlat32), u_xlat32, 1.0);
        u_xlat32 = max(u_xlat32, 0.0);
        u_xlat32 = u_xlat32 * u_xlat32;
        u_xlat32 = u_xlat32 * u_xlat34;
        u_xlat34 = dot(_LightBuffer._AdditionalLightsSpotDir[u_xlati31].xyz, u_xlat7.xyz);
        u_xlat34 = fma(u_xlat34, _LightBuffer._AdditionalLightsAttenuation[u_xlati31].z, _LightBuffer._AdditionalLightsAttenuation[u_xlati31].w);
        u_xlat34 = clamp(u_xlat34, 0.0f, 1.0f);
        u_xlat34 = u_xlat34 * u_xlat34;
        u_xlat32 = u_xlat32 * u_xlat34;
        u_xlat34 = dot(u_xlat0.xyz, u_xlat7.xyz);
        u_xlat34 = clamp(u_xlat34, 0.0f, 1.0f);
        u_xlat32 = u_xlat32 * u_xlat34;
        u_xlat8.xyz = float3(u_xlat32) * _LightBuffer._AdditionalLightsColor[u_xlati31].xyz;
        u_xlat6.xyz = fma(u_xlat6.xyz, float3(u_xlat33), u_xlat1.xyz);
        u_xlat31 = dot(u_xlat6.xyz, u_xlat6.xyz);
        u_xlat31 = max(u_xlat31, 1.17549435e-38);
        u_xlat31 = rsqrt(u_xlat31);
        u_xlat6.xyz = float3(u_xlat31) * u_xlat6.xyz;
        u_xlat31 = dot(u_xlat0.xyz, u_xlat6.xyz);
        u_xlat31 = clamp(u_xlat31, 0.0f, 1.0f);
        u_xlat32 = dot(u_xlat7.xyz, u_xlat6.xyz);
        u_xlat32 = clamp(u_xlat32, 0.0f, 1.0f);
        u_xlat31 = u_xlat31 * u_xlat31;
        u_xlat31 = fma(u_xlat31, -0.9375, 1.00001001);
        u_xlat32 = u_xlat32 * u_xlat32;
        u_xlat31 = u_xlat31 * u_xlat31;
        u_xlat32 = max(u_xlat32, 0.100000001);
        u_xlat31 = u_xlat31 * u_xlat32;
        u_xlat31 = u_xlat31 * 3.0;
        u_xlat31 = 0.0625 / u_xlat31;
        u_xlat6.xyz = fma(float3(u_xlat31), float3(0.0399999991, 0.0399999991, 0.0399999991), u_xlat4.xyz);
        u_xlat5.xyz = fma(u_xlat6.xyz, u_xlat8.xyz, u_xlat5.xyz);
        u_xlati30 = u_xlati30 + 0x1;
    }
    output.SV_Target0.xyz = fma(u_xlat3.xyz, float3(u_xlat28), u_xlat5.xyz);
    output.SV_Target0.w = u_xlat11.x;
    return output;
}
                              _LightBuffer0        _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                    0      _AdditionalLightsColor                   0     _AdditionalLightsAttenuation                 0     _AdditionalLightsSpotDir                 0         UnityPerCamera  �         _Time                                UnityPerDrawp        unity_LightData                   �      unity_LightIndices                   �      unity_SpecCube0_HDR                   �          UnityPerMaterial0         Color_19C6C157                           Vector2_6925FF1E                        Vector1_6DC2C68C                        Color_E448CF9F                                  unity_SpecCube0                   Texture2D_48882336                  _LightBuffer              UnityPerCamera               UnityPerDraw             UnityPerMaterial          