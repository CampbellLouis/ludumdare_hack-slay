﻿using UnityEngine;

namespace Player
{
    public class ThirdPersonCamera : MonoBehaviour
    {
        public Transform lookAt;
        public Transform camTransform;

        private Camera _cam;

        private float distance = 10.0f;
        private float _currentX = 0.0f;
        private float _currentY = 0.0f;
        private float _sensivityX = 4.0f;
        private float _sensivityY = 1.0f;

        void Start()
        {
            camTransform = transform;
            _cam = Camera.main;
        }

        private void Update()
        {
            _currentX += Input.GetAxis("Mouse X");
            _currentY += Input.GetAxis("Mouse Y");
        }

        void LateUpdate()
        {
            Vector3 dir = new Vector3(0, 0, -distance);
            Quaternion rotation = Quaternion.Euler(_currentY, _currentX, 0);
            camTransform.position = lookAt.position + rotation * dir;
        }
    }
}
