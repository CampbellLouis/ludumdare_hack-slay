﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetterPC : MonoBehaviour
{
    //OBJECTS
    public Transform cam;
    CharacterController mover;
    public GameObject spell;

    //CAMEERA
    Vector3 camF;
    Vector3 camR;

    //INPUT
    Vector2 input;

    //PHYSICS
    Vector3 intent;
    Vector3 velocityXZ;
    Vector3 velocity;
    float speed = 5;
    float accel = 11;
    float turnSpeed = 5;
    float turnSpeedLow = 7;
    float turnSpeedHigh = 20;

    //GRAVITY
    float grav = 10;
    bool grounded = false;

    //ATTRIBUTES
    public const int MAX_HEALTH = 99;
    public int health = 0;

    private void Start()
    {
        mover = GetComponent<CharacterController>();
    }

    private void Update()
    {
        DoInput();
        CalculateCamera();
        CalculatedGrounded();
        DoMove();
        DoGravity();
        //DoJump();
        SpawnSpell();

        mover.Move(velocity * Time.deltaTime);
    }

    private void DoInput()
    {
        input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        input = Vector2.ClampMagnitude(input, 1);
    }

    private void CalculateCamera()
    {
        camF = cam.forward;
        camR = cam.right;

        camF.y = 0;
        camR.y = 0;

        camF = camF.normalized;
        camR = camR.normalized;
    }

    private void CalculatedGrounded()
    {
        RaycastHit hit;
        //Debug.DrawRay(transform.position, Vector3.down * 1.1f, Color.red, 1f);
        //Debug.Log("HitGrounded");
        grounded = Physics.Raycast(transform.position, Vector3.down, out hit, 1.1f);
    }

    private void DoMove()
    {
        intent = camF * input.y + camR * input.x;

        float tS = velocity.magnitude / speed;
        turnSpeed = Mathf.Lerp(turnSpeedHigh, turnSpeedLow, tS);
        if (input.magnitude > 0)
        {
            Quaternion rot = Quaternion.LookRotation(intent);
            transform.rotation = Quaternion.Lerp(transform.rotation, rot, turnSpeed * Time.deltaTime);
        }

        velocityXZ = velocity;
        velocityXZ.y = 0;
        velocityXZ = Vector3.Lerp(velocityXZ, intent.magnitude * speed * transform.forward, accel * Time.deltaTime);
        velocity = new Vector3(velocityXZ.x, velocity.y, velocityXZ.z);
    }

    private void DoGravity()
    {

        if (grounded)
        {
            velocity.y = -0.5f;
        }
        else
        {
            velocity.y -= grav * Time.deltaTime;
        }

        velocity.y = Mathf.Clamp(velocity.y, -10, 10);
    }

    private void DoJump()
    {
        if (grounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                velocity.y = 5;
                grounded = false;
            }

        }
    }

    private void SpawnSpell()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(spell, transform.position + Vector3.forward * 1.5f, transform.rotation);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Spell"))
        {
            TakeLife(collision.gameObject.GetComponent<spellMove>().damage);
            Destroy(collision.gameObject);
        }
    }

    private void TakeLife(int damage)
    {
        if (damage > health)
        {
            GameOver();
        }
        else
        {
            health -= damage;
        }
    }

    private void GameOver()
    {
        Destroy(gameObject);
    }
}
