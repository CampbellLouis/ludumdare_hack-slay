﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public const int MAX_HEALTH = 99;

    public float speed = 5;

    private int health = 0;
    public Camera GameCamera;


    private Rigidbody rb;
    public float jumpForce = 7;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float moveVertical = Input.GetAxis("Vertical");
        
        Vector3 camDir = GameCamera.transform.TransformDirection(Vector3.forward);

        if (moveVertical != 0)
        {
            if (moveVertical < 0)
            {
                camDir *= -1;
            }
            rb.MovePosition(transform.position + camDir.normalized * speed * Time.deltaTime);
        }
            
        


        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * jumpForce * 75, ForceMode.Impulse);
        }
        Quaternion CharacterRotation = GameCamera.transform.rotation;
        CharacterRotation.x = 0;
        CharacterRotation.z = 0;

        transform.rotation = CharacterRotation;


    }

    void RemoveHealth(int amount)
    {
        if (amount > health)
        {
            health = 0;
        }
        else
        {
            health -= amount;
        }
    }

    void AddHealth(int amount)
    {
        if (health + amount > MAX_HEALTH)
        {
            health = MAX_HEALTH;
        }
        health += amount;
    }
}
