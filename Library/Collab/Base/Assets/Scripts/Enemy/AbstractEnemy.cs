﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class AbstractEnemy : MonoBehaviour
{
    //OBJECTS
    protected Transform target;
    protected NavMeshAgent agent;
    
    //ATTRIBUTES
    public int health = 10;
    public int damage = 2;
    public float lookRadius = 10f;

    //TIMERS
    public float timeForNextSwing = 5;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        timeForNextSwing -= Time.deltaTime;

        GoToPlayer();
        Attack();

        Destroy();
    }

    protected void GoToPlayer()
    {
        float distance = Vector3.Distance(transform.position, target.transform.position);

        if (distance<=lookRadius)
        {
            agent.SetDestination(target.position);
            if (distance <= agent.stoppingDistance)
            {
                FocusOnPlayer();
                //Attack();

            }
        }
    }

    protected void Destroy()
    {
        if (true)
        {
            if (health <=0)
            {
                Destroy(gameObject);
            }
        }
    }

    protected abstract void Attack();

    protected void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Spell")
        {
            health -= collision.gameObject.GetComponent<spellMove>().damage;
            Destroy(collision.gameObject);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
