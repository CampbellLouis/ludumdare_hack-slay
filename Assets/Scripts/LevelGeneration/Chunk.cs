﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace LevelGeneration
{

    public class Chunk : MonoBehaviour
    {
        public NavMeshSurface surface;
        public enum TileType
        {
            BlockedLeft,
            BlockedRight,
            BlockedTop,
            BlockedBottom,
            CornerTopRight,
            CornerTopLeft,
            CornerBottomLeft,
            CornerBottomRight,
            River,
            RiverEnd,
            Open,
            Closed
        }
        
        [Header("Setup Chunk generation Settings")]
        public TileType type;
        public List<Chunk> compatibleChunks;
        
        
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
