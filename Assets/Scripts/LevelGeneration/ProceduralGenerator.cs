﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


namespace LevelGeneration
{
    public class ProceduralGenerator : MonoBehaviour
    {
        public int size = 10;
        public int chunkSize = 10;
        public List<GameObject> chunks;
        public List<GameObject> chunksTop;
        public List<GameObject> chunksBottom;
        public List<GameObject> chunksLeft;
        public List<GameObject> chunksRight;
        public List<GameObject> chunksCornerTopRight;
        public List<GameObject> chunksCornerTopLeft;
        public List<GameObject> chunksCornerBottomRight;
        public List<GameObject> chunksCornerBottomLeft;


        // Start is called before the first frame update
        void Start()
        {
            chunksTop = chunks.FindAll(c => c.GetComponent<Chunk>().type == Chunk.TileType.BlockedTop);
            chunksBottom = chunks.FindAll(c => c.GetComponent<Chunk>().type == Chunk.TileType.BlockedBottom);
            chunksLeft = chunks.FindAll(c => c.GetComponent<Chunk>().type == Chunk.TileType.BlockedLeft);
            chunksRight = chunks.FindAll(c => c.GetComponent<Chunk>().type == Chunk.TileType.BlockedRight);
            chunksCornerTopRight = chunks.FindAll(c => c.GetComponent<Chunk>().type == Chunk.TileType.CornerTopRight);
            chunksCornerTopLeft = chunks.FindAll(c => c.GetComponent<Chunk>().type == Chunk.TileType.CornerTopLeft);
            chunksCornerBottomRight =
                chunks.FindAll(c => c.GetComponent<Chunk>().type == Chunk.TileType.CornerBottomRight);
            chunksCornerBottomLeft =
                chunks.FindAll(c => c.GetComponent<Chunk>().type == Chunk.TileType.CornerBottomLeft);
            this.size -= 1;
            GameObject block = null;
            for (int x = 0; x <= size; x++)
            {
                for (int z = 0; z <= size; z++)
                {
                    if (block != null)
                    {
                        // Top Right corner piece
                        if (x == size && z == size)
                        {
                            block = Instantiate(chunksCornerTopRight[Random.Range(0, chunksCornerTopRight.Count)],
                                new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                        }

                        // Top Left corner piece
                        else if (x == 0 && z == size)
                        {
                            block = Instantiate(chunksCornerTopLeft[Random.Range(0, chunksCornerTopLeft.Count)],
                                new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                        }

                        // Bottom Right
                        else if (x == size && z == 0)
                        {
                            block = Instantiate(chunksCornerBottomRight[Random.Range(0, chunksCornerBottomRight.Count)],
                                new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                        }
                        // Top Left Bottom corner piece
                        else if (x == 0 && z == 0)
                        {
                            block = Instantiate(chunksCornerBottomLeft[Random.Range(0, chunksCornerBottomLeft.Count)],
                                new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                        }
                        //Left piece
                        else if (x == 0)
                        {
                            block = Instantiate(chunksLeft[Random.Range(0, chunksLeft.Count)],
                                new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                        }
                        //Right piece
                        else if (x == size)
                        {
                            block = Instantiate(chunksRight[Random.Range(0, chunksRight.Count)],
                                new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                        }
                        //Bottom piece
                        else if (z == 0)
                        {
                            block = Instantiate(chunksBottom[Random.Range(0, chunksBottom.Count)],
                                new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                        }
                        //Top piece
                        else if (z == size)
                        {
                            block = Instantiate(chunksTop[Random.Range(0, chunksTop.Count)],
                                new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.Euler(0,90,0));
                        }
                        else
                        {
                            switch (block.GetComponent<Chunk>().type)
                            {
                                default:
                                {
                                    block = Instantiate(chunks[Random.Range(0, chunks.Count)],
                                        new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        block = Instantiate(chunksCornerBottomLeft[Random.Range(0, chunksCornerBottomLeft.Count)],
                            new Vector3(x * chunkSize, 0, z * chunkSize), Quaternion.identity);
                    }
                }
                if (block != null)
                {
                    block.GetComponent<Chunk>().surface.BuildNavMesh();
                }
            }
        }
    }
}