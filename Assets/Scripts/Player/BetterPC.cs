﻿using UnityEngine;

namespace Player
{
    public class BetterPC : MonoBehaviour
    {
        //OBJECTS
        public Transform cam;
        CharacterController _mover;
        public GameObject spell;
        private Animator _animator;

        //CAMEERA
        Vector3 _camF;
        Vector3 _camR;

        //INPUT
        Vector2 _input;

        //PHYSICS
        Vector3 _intent;
        Vector3 _velocityXz;
        Vector3 _velocity = Vector3.zero;
        float speed = 5;
        float accel = 11;
        float _turnSpeed = 2;
        float turnSpeedLow = 2;
        float turnSpeedHigh = 10;

        //GRAVITY
        float grav = 10;
        bool _grounded = false;

        //ATTRIBUTES
        public const int MaxHealth = 99;
        public int health = 0;
        private static readonly int Speed = Animator.StringToHash("speed");

        private void Start()
        {
            _mover = GetComponent<CharacterController>();
            _animator = GetComponent<Animator>();
        }

        private void Update()
        {
            DoInput();
            CalculateCamera();
            CalculatedGrounded();
            DoMove();
            DoGravity();
            //DoJump();
            SpawnSpell();

            _mover.Move(_velocity * Time.deltaTime);
        }

        private void DoInput()
        {
            _input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            //Debug.Log(input);
            _input = Vector2.ClampMagnitude(_input, 1);
        }

        private void CalculateCamera()
        {
            _camF = cam.forward;
            _camR = cam.right;

            _camF.y = 0;
            _camR.y = 0;

            _camF = _camF.normalized;
            _camR = _camR.normalized;
        }

        private void CalculatedGrounded()
        {
            //Debug.DrawRay(transform.position, Vector3.down * 1.1f, Color.red, 1f);
            //Debug.Log("HitGrounded");
            _grounded = Physics.Raycast(transform.position, Vector3.down, out _, 1.1f);
        }

        private void DoMove()
        {
            _animator.SetFloat(Speed,_input.y);
            if (_input.y >= 0)
            {
                
                _intent = _camF * _input.y + _camR * _input.x;
                //Debug.Log(intent.magnitude);
                if (_velocity != Vector3.zero)
                {
                    float tS = _velocity.magnitude / speed;
                    _turnSpeed = Mathf.Lerp(turnSpeedHigh, turnSpeedLow, tS);
                    if (_input.magnitude > 0)
                    {
                        Quaternion rot = Quaternion.LookRotation(_intent);
                        transform.rotation = Quaternion.Lerp(transform.rotation, rot, _turnSpeed * Time.deltaTime);
                    }
                }
                _velocityXz = _velocity;
                _velocityXz.y = 0;
                _velocityXz = Vector3.Lerp(_velocityXz, _intent.magnitude * speed * transform.forward, accel * Time.deltaTime);
                _velocity = new Vector3(_velocityXz.x, _velocity.y, _velocityXz.z);
            }

        }

        private void DoGravity()
        {

            if (_grounded)
            {
                _velocity.y = -0.5f;
            }
            else
            {
                _velocity.y -= grav * Time.deltaTime;
            }

            _velocity.y = Mathf.Clamp(_velocity.y, -10, 10);
        }

        private void DoJump()
        {
            if (_grounded)
            {
                if (Input.GetButtonDown("Jump"))
                {
                    _velocity.y = 5;
                    _grounded = false;
                }

            }
        }

        private void SpawnSpell()
        {
            if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("Jump"))
            {
                var transform1 = transform;
                Instantiate(spell, transform1.position + transform1.forward * 2f, transform1.rotation);
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Spell"))
            {
                TakeLife(collision.gameObject.GetComponent<spellMove>().damage);
                Destroy(collision.gameObject);
            }
        }

        private void TakeLife(int damage)
        {
            if (damage > health)
            {
                GameOver();
            }
            else
            {
                health -= damage;
            }
        }

        private void GameOver()
        {
            Destroy(gameObject);
        }
    }
}
