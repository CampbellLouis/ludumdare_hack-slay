﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spellMove : MonoBehaviour
{
    //ATTRIBUTES
    public int speed = 5;
    public float spellsTimeOfLife = 10;
    public int damage = 5;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        spellsTimeOfLife -= Time.deltaTime;
        if (spellsTimeOfLife < 0)
        {
            Destroy(gameObject);
        }
    }
}
