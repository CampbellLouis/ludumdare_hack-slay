﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    public Transform player;
    
    float tilt = 45;
    float camDist = 4;


    private void Update()
    {
        if (player != null)
        {
            transform.position = player.position - transform.forward * camDist;
            transform.rotation = Quaternion.Euler(tilt, player.rotation.eulerAngles.y, 0);
        }

    }
}
