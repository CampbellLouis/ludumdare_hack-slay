﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    public GameObject[] enemy = new GameObject[2];
    private GameObject player;

    public GameObject cam;

    private int count = 0;

    private int max;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        int size = cam.GetComponent<LevelGeneration.ProceduralGenerator>().size;
        int chunkSize = cam.GetComponent<LevelGeneration.ProceduralGenerator>().chunkSize;

        max = size * chunkSize;
    }

    void Update()
    {
        int random = Random.Range(0, 100);
        if (random > 80 && count < 20)
        {
            int enemyAt = Random.Range(0, enemy.Length);
            Instantiate(enemy[enemyAt], new Vector3(Random.Range(0, max), 1.5f, Random.Range(0, max)), player.transform.rotation);
            count++;
        }
    }
}
