﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseRangeEnemy : AbstractEnemy
{
    public float swingdistance = 2f;

    protected override void Attack()
    {
        float distance = Vector3.Distance(transform.position, target.transform.position);
        if (distance <= swingdistance)
        {
            if (timeForNextSwing < 0)
            {
                Debug.Log("Swing");
                timeForNextSwing = 5;
            }
        }
    }

    protected override void GoToPlayer()
    {
        base.GoToPlayer();
    }

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, swingdistance);
    }
}
