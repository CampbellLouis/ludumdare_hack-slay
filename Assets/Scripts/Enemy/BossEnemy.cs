﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossEnemy : CloseRangeEnemy
{
    public float chargeDistance = 5f;
    public float accel = 0.2f;
    

    private Vector3 positionOfPlayer;

    protected override void GoToPlayer()
    {
        float distance = Vector3.Distance(transform.position, target.transform.position);

        if (distance <= lookRadius && distance >= chargeDistance)
        {
            agent.SetDestination(target.position);
            positionOfPlayer = target.position;
            Attack();
        }
        else if (distance < chargeDistance)
        {
            Charge();
        }
    }

    private void Charge()
    {
        agent.ResetPath();
        agent.SetDestination(positionOfPlayer);
        agent.acceleration = 60;
        agent.speed = 20;

        if (Vector3.Distance(positionOfPlayer, transform.position) < 1f)
        {
            Debug.Log("Weed");
            agent.acceleration = 8;
            agent.speed = 3.5f;
        }
    }

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, chargeDistance);
    }
}
