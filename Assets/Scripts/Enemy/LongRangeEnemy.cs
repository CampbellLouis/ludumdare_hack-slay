﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongRangeEnemy : AbstractEnemy
{
    public GameObject spell;
    public float shootRadius = 10f;

    protected override void Attack()
    {
        float distance = Vector3.Distance(transform.position, target.transform.position);
        if (distance <= shootRadius)
        {
            FocusOnPlayer();
            agent.ResetPath();
            if (timeForNextSwing < 0)
            {
                Instantiate(spell, transform.position + transform.forward * 2f, transform.rotation);
                timeForNextSwing = 5;
            }
        }
    }

    protected override void OnDrawGizmosSelected()
    {
        base.OnDrawGizmosSelected();
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, shootRadius);
    }
}
